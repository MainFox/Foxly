# Foxly - telegram userbot
# Copyright (C) 2024-present MainFox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from pyrogram import filters
from pyrogram.types import Message


def start_msg(data):
    async def text_filter(flt, __, m: Message) -> bool:
        try:
            return m.text.startswith(flt.data)
        except:
            return False

    return filters.create(text_filter, data=data)
