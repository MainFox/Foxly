# Foxly - telegram userbot
# Copyright (C) 2024-present MainFox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from pyrogram import Client
from core.loader import loader

test = 0


def starting():
    global test
    if not test:
        test += 1
        return False
    else:
        return True


@Client.on_raw_update(group=1)
async def handler(app, handler, raw, _):
    try:
        if not starting():
            for mod in loader.list():
                if loader.load(mod).info.load_event == 'background':
                    await loader.load(mod).init(app)

        for mod in loader.list():
            if loader.load(mod).info.load_event == 'all':
                await loader.load(mod).init(app, handler, raw)
    except:
        pass
