# Foxly - telegram userbot
# Copyright (C) 2024-present MainFox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import json


class utils:
    me = None


commands = {}


def command(func):
    commands[func.__name__] = func
    return func


def language(func):
    async def wrapper(app, m, me, args):
        tmp = DictToObject(
            json.load(open(f'modules/{func.__name__}/language.json', 'r')))
        language = tmp.ru
        await func(app, m, me, args, language)
    return wrapper


class DictToObject:
    def __init__(self, dictionary):
        self._dictionary = dictionary
        for key, value in dictionary.items():
            if isinstance(value, dict):
                value = DictToObject(value)
            setattr(self, key, value)

    def __getattr__(self, item):
        try:
            return self.__dict__[item]
        except KeyError:
            raise AttributeError(
                f"'DictToObject' object has no attribute '{item}'")
