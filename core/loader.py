# Foxly - telegram userbot
# Copyright (C) 2024-present MainFox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import ast
import os
import json
import importlib
import sys

from typing import List, Dict, Any, Union

allowed_imports = {}
forbidden_functions = {'write', 'exec'}
forbidden_folders = {'FoxlyUB.session'}


class CodeAnalyzer:
    def __init__(self, allowed_imports, forbidden_functions, forbidden_folders):
        self.allowed_imports = allowed_imports
        self.forbidden_functions = forbidden_functions
        self.forbidden_folders = forbidden_folders
        self.results = {"status": "ok", "message": "", "critical": []}

    def check_module(self, module_code):
        tree = ast.parse(module_code)

        for node in ast.walk(tree):
            if isinstance(node, ast.Import):
                for alias in node.names:
                    if alias.name in self.allowed_imports:
                        self.results["critical"].append(
                            {"type": "import", "name": alias.name})

            elif isinstance(node, ast.ImportFrom):
                if node.module in self.allowed_imports:
                    self.results["critical"].append(
                        {"type": "import", "name": node.module})

            elif isinstance(node, ast.Call):

                if isinstance(node.func, ast.Attribute):
                    if isinstance(node.func.value, ast.Call) and isinstance(node.func.value.func, ast.Name) and node.func.value.func.id == '__import__':
                        module_name = node.func.value.args[0].s
                        if module_name in self.allowed_imports:
                            self.results["critical"].append(
                                {"type": "dynamic_import", "name": module_name})

                elif isinstance(node.func, ast.Name) and node.func.id in self.forbidden_functions:
                    self.results["critical"].append(
                        {"type": "function_call", "name": node.func.id})

    def check_file_access(self, module_code):
        tree = ast.parse(module_code)

        for node in ast.walk(tree):
            if isinstance(node, ast.Call) and isinstance(node.func, ast.Name) and node.func.id == 'open':
                if len(node.args) > 0 and isinstance(node.args[0], ast.Str):
                    file_path = node.args[0].s
                    directory = os.path.dirname(file_path)
                    if any(folder in directory for folder in self.forbidden_folders):
                        self.results["critical"].append(
                            {"type": "file_access", "path": directory})

    def check_code(self, module_code):
        self.results["critical"] = []

        self.check_module(module_code)
        self.check_file_access(module_code)

        if self.results["critical"]:
            self.results["status"] = "danger"
            self.results["message"] = "Обнаружены угрозы!"

    def get_results(self):
        return json.dumps(self.results, ensure_ascii=False, indent=4)


class loader:
    @staticmethod
    def list() -> List[str]:
        return [
            mod for mod in os.listdir('./modules')
            if mod != "__pycache__" and os.path.isdir(os.path.join('./modules', mod))
        ]

    @staticmethod
    def load(name: str) -> Union[importlib.machinery.ModuleSpec, Dict[str, Any]]:

        module_code = ''
        module_files = []

        for file in os.listdir(os.path.join('modules', name, 'src')):
            if not file.endswith('.py'):
                continue
            module_files.append(os.path.join('modules', name, 'src', file))

        for file in os.listdir(os.path.join('modules', name)):
            if not file.endswith('.py') and not file.endswith('.json'):
                continue
            if file.endswith('.py'):
                module_files.append(os.path.join('modules', name, file))

        for file_path in module_files:
            with open(file_path, 'r') as f:
                module_code += f.read() + '\n'

        analyzer = CodeAnalyzer(
            allowed_imports, forbidden_functions, forbidden_folders)
        analyzer.check_code(module_code)
        result = analyzer.get_results()

        if json.loads(result)["status"] == "ok":
            try:
                return importlib.import_module(f'modules.{name}')

            except ModuleNotFoundError as e:
                print(f"Module {name} not found: {e}")
                return {}

            except Exception as e:
                print(f"Error loading module {name}: {e}")
                json_path = os.path.join('modules', name, 'main.json')
                if os.path.isfile(json_path):
                    try:
                        with open(json_path, 'r') as json_file:
                            return {'info': json.load(json_file)}
                    except Exception as json_error:
                        print(f"Error reading JSON file for module {name}: {json_error}")
                return {}

        else:
            print(result)
            return {}

    @staticmethod
    def reload(name: str) -> None:

        module = sys.modules[f'modules.{name}']
        importlib.reload(module)

        for attribute_name in dir(module):
            attribute = getattr(module, attribute_name)

            if isinstance(attribute, type(sys)) and attribute.__name__.startswith(f'modules.{name}.src'):
                try:
                    loader.reload(f'{name}.src.{name}')
                except Exception as e:
                    print(f"Error reloading module {name}: {e}")

    @staticmethod
    def reload_all() -> None:
        for mod in loader.list():
            loader.reload(mod)
