# Foxly - telegram userbot
# Copyright (C) 2024-present MainFox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from pyrogram import Client

app = Client(
    "FoxlyUB",

    api_id=20602236,
    api_hash="d1f41c2c090e3be2bd66704887ff314a",

    app_version="0.1",
    device_model="PC",
    system_version="Windows 10",
    lang_pack="tdesktop",
    lang_code="ru",
    system_lang_code="ru",

    plugins=dict(
        root="plugins"
    ),
)

app.run()
