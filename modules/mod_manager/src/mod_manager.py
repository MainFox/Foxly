# Foxly - telegram userbot
# Copyright (C) 2024-present MainFox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from core.utils import command, language
from core.loader import loader


@command
async def list(app, m, me, args, language):
    out = ''
    for mod in loader.list():
        module_info = loader.load(mod).info
        if module_info:
            out += f'• **{module_info.module_name}** (`{mod}`)\n' \
                f'╰> `{"`, `".join(module_info.module_commands) if module_info.module_commands else "нет команд"}\n`'
    await m.edit('📋 **Список доступных модулей:**' + '\n\n' + out)


@command
async def reload(app, m, me, args, language):
    loader.reload_all()
    await m.edit('**Перезагрузка модулей завершена.**')


@command
async def help(app, m, me, args, language):
    try:
        module_info = loader.load(args[0]).info
        if module_info:
            msg = f'''
📦 **Модуль:** `{module_info.module_name}`
ℹ️ **Описание:** `{module_info.module_info}`
⚙️ **Команды:** `{"`, `".join(module_info.module_commands) if module_info.module_commands else "нет команд"}`
			'''
            await m.edit(msg)
        else:
            await m.edit(f"Модуль `{args[1]}` не найден или произошла ошибка при загрузке.")
    except Exception as e:
        print(str(e))
