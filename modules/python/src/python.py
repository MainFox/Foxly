# Foxly - telegram userbot
# Copyright (C) 2024-present MainFox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import sys
import traceback

from meval import meval
from io import StringIO

from core.utils import command


@command
async def eval(app, m, me, args, language):
    try:

        old_stdout = sys.stdout
        sys.stdout = mystdout = StringIO()

        await meval(' '.join(args), globals(), app=app, m=m, reply=m.reply_to_message, me=me)

        await m.edit(
            f'🐍 <b>Code</b>\n'
            f'<pre language=python>{" ".join(args)}</pre>\n'
            f'💻 <b>Result:</b>\n'
            f'<pre language=python>{mystdout.getvalue()}</pre>'
        )

    except:

        sys.stdout = old_stdout
        await m.edit(
            f'🐍 <b>Code:</b>\n'
            f'<pre language=python>{" ".join(args)}</pre>\n'
            f'💻 <b>Traceback:</b>\n'
            f'<pre language=python>{traceback.format_exc()}</pre>'
        )
