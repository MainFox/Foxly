# Foxly - telegram userbot
# Copyright (C) 2024-present MainFox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from core.utils import command

from .code_block import code_block


@command
async def pcode(app, m, me, args, language):

    if m.reply_to_message:

        await m.edit('**Generation...**')

        await app.send_document(
            m.chat.id,
            code_block(
                m.reply_to_message.text,
                args[0],
                line_numbers=False
            ),
            reply_to_message_id=m.reply_to_message.id
        )

    else:

        await m.edit('**Generation...**')

        await app.send_document(
            m.chat.id,
            code_block(
                m.text.replace(f'.pcode {args[0]} ', ''),
                args[0],
                line_numbers=False
            )
        )

        await m.delete()
