# Foxly - telegram userbot
# Copyright (C) 2024-present MainFox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import io

from pygments.formatters import JpgImageFormatter
from pygments import highlight
from pygments import lexers
from PIL import Image, ImageDraw


def code_block(code, language, style='dracula', font_size=50, line_numbers=True):

    fmt = JpgImageFormatter(style=style, font_size=font_size, line_numbers=line_numbers,
                            line_number_bg=(40, 42, 54), line_number_fg=(100, 100, 100))

    lex = lexers.get_lexer_by_name(language)
    code = Image.open(io.BytesIO(highlight(code, lex, fmt)))

    center = int((code.size[0]+300-code.size[0]-35) /
                 2), int((code.size[1]+300-code.size[1]-105)/2)

    img = Image.new(
        'RGB', (code.size[0]+300, code.size[1]+300), (175, 238, 238))
    idraw = ImageDraw.Draw(img)

    idraw.rounded_rectangle(
        (center, (code.size[0]+70+center[0], code.size[1]+120+center[1])), 20, fill=(40, 42, 54))

    img.paste(code, (center[0]+20, center[1]+75))

    idraw.rounded_rectangle(
        ((center[0]+15, center[1]+15), (center[0]+47, 47+center[1])), 100, fill=(255, 85, 85))
    idraw.rounded_rectangle(
        ((center[0]+62, center[1]+15), (center[0]+94, 47+center[1])), 100, fill=(241, 250, 140))
    idraw.rounded_rectangle(
        ((center[0]+109, center[1]+15), (center[0]+141, 47+center[1])), 100, fill=(80, 250, 123))

    tmp = io.BytesIO()
    tmp.name = 'code.png'

    img.save(tmp, format='PNG')

    return tmp
