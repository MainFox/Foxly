# Foxly - telegram userbot
# Copyright (C) 2024-present MainFox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import json

from .src.bash import *

from core.utils import DictToObject, language, commands

info = DictToObject(
    json.load(
        open(f'{__path__[0]}/main.json', 'r')
    )
)


async def init(app, m, me, args) -> None:
    command_name = args[0][1:]
    if command_name in info.module_commands:
        await commands[command_name](app, m, me, args[1:], language)
